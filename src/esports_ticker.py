from AlexaSkill import AlexaSkill
from abiosgaming.client import AbiosClient
from abiosgaming.match import Match
import logging
import dateutil
import dateutil.parser
from dateutil.relativedelta import relativedelta
import datetime
import pytz

log = logging.getLogger()
log.setLevel(logging.DEBUG)

games_map = {
    'DOTA 2': 1,
    'League of Legends': 2,
    'Starcraft 2': 3,
    'Heroes of Newerth': 4,
    'Counter-strike': 5,
    'Hearthstone': 6,
    'World of Warcraft': 7,
    'SMITE': 8,
    'Heroes of the Storm': 9,
    'Super Smash Bros': 10,
    'Call of Duty': 11
}

speech_aliases = {
    'dota 2': 'DOTA 2',
    'league of legends': 'League of Legends',
    'starcraft 2': 'Starcraft 2',
    'heroes of newerth': 'Heroes of Newerth',
    'counter strike': 'Counter Strike',
    'hearthstone': 'Hearthstone',
    'world of warcraft': 'World of Warcraft',
    'smite': 'SMITE',
    'heroes of the storm': 'Heroes of the Storm',
    'super smash bros': 'Super Smash Bros',
    'call of duty': 'Call of Duty',
    'go to to': 'DOTA 2',
    'heartstone': 'Hearthstone',
    'league of legend': 'League of Legends',
    'counterstrike': 'Counter-strike',
    'heart of stone': 'Hearthstone',
    'hearth stone': 'Hearthstone',
    'deal did to': 'DOTA 2',
    'do to to': 'DOTA 2',
    'do to two': 'DOTA 2'
}

ITEMS_PER_RESPONSE = 5


class EsportsTickerAlexaSkill(AlexaSkill):
    def intentHandler(self):
        print "I'm handling the intent {}".format(self.request.type)
        intentTypes = {
            'GetUpcomingMatchesIntent': lambda x: handleUpcomingMatchesIntent(x),
            'StopIntent': lambda x: handleStopIntent(x),
            'MoreIntent': lambda x: handleMoreIntent(x),
            'GetCurrentMatchesIntent': lambda x: handleCurrentMatchesIntent(x),
            'GetRecentResultsIntent': lambda x: handleRecentResultsIntent(x),
        }
        print self.request.intent.name
        if self.request.intent.name in intentTypes:
            return intentTypes[self.request.intent.name](self)
        else:
            print "WTF happened??? {}".format(self.request.intent)

    def launchHandler(self):
        print "{} Launch".format(self.__class__.__name__)
        speechOutput = "<speak>Opened esports ticker.  Would you like to hear about recent results, current matches, or upcoming matches?</speak>"
        repromptText = "Hello.  Would like like to know about esports?"
        return self.ask(speechOutput, repromptText)

    def sessionEndHandler(self):
        print "{} Session End".centerformat(self.__class__.__name__)


def handleStopIntent(EsportsTicker):
    return EsportsTicker.tell("<speak>Thanks for using the esports ticker</speak>")


def get_game_id_from_game_name(game):
    print "I heard {}".format(game)
    try:
        return games_map[speech_aliases[game]]
    except KeyError:
        print "I heard {}".format(game)
        return None


def get_competitors_list(competitors, name_type="name"):
    competitor_list = []

    for competitor in competitors:
        if competitor is not None:
            if competitor[name_type]:
                competitor_list.append(competitor[name_type])
            else:
                competitor_list.append(get_default_competitor())
        else:
            competitor_list.append(get_default_competitor())
    return competitor_list


def get_default_competitor(name_type="name"):
    if name_type == "short_name":
        return "UNK"
    return "Unknown"


def get_competitors_speech_string(match, with_results=False):
    matchup_string = ''
    matchup = match._raw_data['matchups'][0]
    if matchup['competitors']:
        competitor_list = get_competitors_list(matchup['competitors'])
        log.debug("Competitor list: {}".format(competitor_list))
        if with_results:
            scores = match.get_score().items()
            matchup_string = "{} defeated {} with a score of {} to {}".format(scores[0][0], scores[1][0], scores[0][1], scores[1][1])
        else:
            matchup_string = ' <break strength=\"medium\"/> versus <break strength=\"medium\"/> '.join(competitor_list)
    print "--- raw matchup string {}".format(matchup_string)
    if not matchup_string:
        print "--- am I gettinging in competitor speech string?"
        matchup_string = ' unknown matchup '
    return matchup_string


def get_competitors_card_string(match, with_results=False):
    matchup_string = ''
    matchup = match._raw_data['matchups'][0]
    if matchup['competitors']:
        if with_results:
            scores = match.get_score(short_name=True).items()
            matchup_string = "{} vs. {}   {}-{}".format(scores[0][0], scores[1][0], scores[0][1], scores[1][1])
        else:
            competitor_list = get_competitors_list(matchup['competitors'], name_type="short_name")
            matchup_string = ' vs. '.join(competitor_list)
    if not matchup_string:
        matchup_string = ' Unknown Matchup '
    return matchup_string


def get_intent_slot_value(slots, name):
    if name == 'Game':
        try:
            return slots.Game.value
        except AttributeError:
            return None
    raise "Unknown slot type"


def update_pagination_session(EsportsTicker, next_page, list_of_responses, cardTitle=None, game_name=None, add_duration=False, add_results=False):
    if not EsportsTicker.session.has_key('attributes'):
        EsportsTicker.session['attributes'] = {}
    EsportsTicker.session['attributes']['pagination_remainder'] = list_of_responses
    EsportsTicker.session['attributes']['next_page'] = next_page
    EsportsTicker.session['attributes']['card_title'] = cardTitle
    if game_name:
        EsportsTicker.session['attributes']['game_name'] = game_name
    if add_duration:
        EsportsTicker.session['attributes']['add_duration'] = "1"
    if add_results:
        EsportsTicker.session['attributes']['add_results'] = "1"


def handleMoreIntent(EsportsTicker):
    log.debug("In handleMoreIntent")
    log.debug("Session: {}".format(EsportsTicker.session))
    shouldEndSession = True
    if not EsportsTicker.session['attributes']['pagination_remainder'] and not EsportsTicker.session['attributes']['next_page']:
        return EsportsTicker.tell("<speak>That was the end of the information I have.</speak>")


    addGameName = False
    if EsportsTicker.session['attributes'].has_key('game_name'):
        addGameName = True

    addDuration = False
    if EsportsTicker.session['attributes'].has_key('add_duration'):
        addDuration = True

    addResults = False
    if EsportsTicker.session['attributes'].has_key('add_results'):
        addResults = True

    speechOutput = '<speak>'
    cardOutput = ''
    cardTitle = EsportsTicker.session['attributes']['card_title'] + " Cont."
    if len(EsportsTicker.session['attributes']['pagination_remainder']) > ITEMS_PER_RESPONSE:
        log.info("Enough speech left in session cache")
        speechOutput += ' '.join(x[0] for x in EsportsTicker.session['attributes']['pagination_remainder'][:ITEMS_PER_RESPONSE])
        cardOutput += '\n'.join(x[1] for x in EsportsTicker.session['attributes']['pagination_remainder'][:ITEMS_PER_RESPONSE])
        update_pagination_session(
            EsportsTicker,
            EsportsTicker.session['attributes']['next_page'],
            EsportsTicker.session['attributes']['pagination_remainder'][ITEMS_PER_RESPONSE:],
            EsportsTicker.session['attributes']['card_title']
        )
        shouldEndSession = False
    elif(len(EsportsTicker.session['attributes']['pagination_remainder']) > 0
         and len(EsportsTicker.session['attributes']['pagination_remainder']) <= ITEMS_PER_RESPONSE
         and not EsportsTicker.session['attributes']['next_page']):
        log.info("At the end of the line, close off speech")
        speechOutput += ' '.join(x[0] for x in EsportsTicker.session['attributes']['pagination_remainder'][:ITEMS_PER_RESPONSE])
        cardOutput += '\n'.join(x[1] for x in EsportsTicker.session['attributes']['pagination_remainder'][:ITEMS_PER_RESPONSE])
    elif(len(EsportsTicker.session['attributes']['pagination_remainder']) < ITEMS_PER_RESPONSE
         and EsportsTicker.session['attributes']['next_page']):
        amount_needed = ITEMS_PER_RESPONSE - len(EsportsTicker.session['attributes']['pagination_remainder'])
        log.info("I need {} more items".format(amount_needed))
        client = getClient()
        data = client._paginated_call(EsportsTicker.session['attributes']['next_page'], count=amount_needed)
        matches = [Match(match) for match in data]
        dataList = get_match_speech_and_card(matches, addGameName, addDuration, addResults)
        EsportsTicker.session['attributes']['pagination_remainder'].extend(dataList)
        speechOutput += ' '.join(x[0] for x in EsportsTicker.session['attributes']['pagination_remainder'])
        cardOutput += '\n'.join(x[1] for x in EsportsTicker.session['attributes']['pagination_remainder'])

        # pagination remiander
        remainder_matches = [Match(match) for match in client.pagination_remainder]
        remainder_list = get_match_speech_and_card(remainder_matches, addGameName, addDuration, addResults)

        if (remainder_list or client.next_page):
            update_pagination_session(
                EsportsTicker,
                client.next_page,
                remainder_list,
                cardTitle=EsportsTicker.session['attributes']['card_title']
            )
            shouldEndSession = False

    elif not EsportsTicker.session['attributes']['pagination_remainder']:
        log.info("No remaining speech need to get more")
        client = getClient()
        data = client._paginated_call(EsportsTicker.session['attributes']['next_page'], count=ITEMS_PER_RESPONSE)
        matches = [Match(match) for match in data]
        dataList = get_match_speech_and_card(matches, addGameName, addDuration, addResults)
        speechOutput += ' '.join(x[0] for x in dataList)
        cardOutput += '\n'.join(x[1] for x in dataList)

        # pagination remiander
        remainder_matches = [Match(match) for match in client.pagination_remainder]
        remainder_list = get_match_speech_and_card(remainder_matches, addGameName, addDuration, addResults)

        if (remainder_list or client.next_page):
            update_pagination_session(
                EsportsTicker,
                client.next_page,
                remainder_list,
                EsportsTicker.session['attributes']['card_title']
            )
            shouldEndSession = False
    else:
        return EsportsTicker.tell("<speak>I got lost somewhere... not sure what to do</speak>")
    if not shouldEndSession:
        speechOutput += '<break strength=\"medium\"/>Would you like to hear more?'
    speechOutput += '</speak>'
    return EsportsTicker.tellWithCard(speechOutput, cardTitle, cardOutput, shouldEndSession=shouldEndSession, repromptText="Would you like to hear more matches?")


def handleCurrentMatchesIntent(EsportsTicker):
    game_name = get_intent_slot_value(EsportsTicker.request.intent.slots, "Game")
    log.debug("game name: {}".format(game_name))
    NO_MATCH_SPEECH = 'I could not find any live matches'
    client = getClient()
    params = {}
    params['addons'] = ['tournament', 'matchups']

    game_id = None
    # make sure we can get the game value
    cardTitle = "Current Matches"
    if game_name:
        game_id = get_game_id_from_game_name(game_name)
        if game_id:
            params['games'] = [game_id]
            cardTitle += "- {}".format(speech_aliases[game_name])
            NO_MATCH_SPEECH += "for {}".format(speech_aliases[game_name])

    matches = client.get_current_matches(**params)
    remainder_matches = [Match(data) for data in client.pagination_remainder]
    remainder_list = get_match_speech_and_card(remainder_matches, game_name)
    update_pagination_session(EsportsTicker, client.next_page, remainder_list, cardTitle=cardTitle, game_name=game_name)
    speechOutput = '<speak>'
    cardOutput = ""
    matchOutputList = get_match_speech_and_card(matches, game_name)
    print matchOutputList
    if matchOutputList:
        speechOutput += ' '.join([m[0] for m in matchOutputList])
        cardOutput += "\n".join([m[1] for m in matchOutputList])
    else:
        return EsportsTicker.tell("<speak>{}</speak>".format(NO_MATCH_SPEECH))
    shouldEndSession = True
    if not client.pagination_max_items:
        shouldEndSession = False
        speechOutput += "<break strength=\"medium\"/>Would you like to hear more?"
    speechOutput += "</speak>"
    return EsportsTicker.tellWithCard(speechOutput, cardTitle, cardOutput, shouldEndSession=shouldEndSession, repromptText="Would you like to hear more matches?")

def getClient():
    return AbiosClient(client_id='iJvR6ygRiaWz3klmvqvOg66WvuYC6hnSFfKIqs1I', secret='TCfZ6VFaTbOOedHdsytjbMRanEcCUpG9ZXQsCB0m')

def handleRecentResultsIntent(EsportsTicker):
    game_name = get_intent_slot_value(EsportsTicker.request.intent.slots, "Game")
    print "game name: {}".format(game_name)
    client = getClient()
    params = {}
    params['addons'] = ['tournament', 'matchups']

    game_id = None
    # make sure we can get the game value
    cardTitle = "Recent Results"
    if game_name:
        game_id = get_game_id_from_game_name(game_name)
        if game_id:
            params['games'] = [game_id]
            cardTitle += "- {}".format(speech_aliases[game_name])
    log.info("Get recent matches")
    matches = client.get_recent_results(**params)
    log.info("Matches {}".format(matches))
    log.info("Remainder {}".format(client.pagination_remainder))
    remainder_matches = [Match(data) for data in client.pagination_remainder]
    log.info("Remainder matches {}".format(remainder_matches))
    remainder_list = get_match_speech_and_card(
                                        remainder_matches,
                                        game_name,
                                        False,
                                        True)
    update_pagination_session(EsportsTicker, client.next_page, remainder_list, cardTitle=cardTitle, game_name=game_name, add_duration=False, add_results=True)
    speechOutput = '<speak>'
    cardOutput = ""
    matchOutputList = get_match_speech_and_card(matches, game_name, False, True)
    print matchOutputList
    speechOutput += ' '.join([m[0] for m in matchOutputList])
    cardOutput += "\n".join([m[1] for m in matchOutputList])
    speechOutput += "<break strength=\"medium\"/>Would you like to hear more?</speak>"
    return EsportsTicker.tellWithCard(speechOutput, cardTitle, cardOutput, shouldEndSession=False, repromptText="Would you like to hear more matches?")


def handleUpcomingMatchesIntent(EsportsTicker):
    # check if there is a game value in a slot
    game_name = get_intent_slot_value(EsportsTicker.request.intent.slots, "Game")
    print "game name: {}".format(game_name)
    client = getClient()
    params = {}
    params['addons'] = ['tournament', 'matchups']

    game_id = None
    # make sure we can get the game value
    cardTitle = "Upcoming Matches"
    if game_name:
        game_id = get_game_id_from_game_name(game_name)
        if game_id:
            params['games'] = [game_id]
            cardTitle += "- {}".format(speech_aliases[game_name])
    log.info("Get upcoming matches")
    matches = client.get_upcoming_matches(**params)
    log.info("Matches {}".format(matches))
    log.info("Remainder {}".format(client.pagination_remainder))
    remainder_matches = [Match(data) for data in client.pagination_remainder]
    log.info("Remainder matches {}".format(remainder_matches))
    remainder_list = get_match_speech_and_card(
                                        remainder_matches,
                                        game_name,
                                        True)
    update_pagination_session(EsportsTicker, client.next_page, remainder_list, cardTitle=cardTitle, game_name=game_name, add_duration=True)
    speechOutput = '<speak>'
    cardOutput = ""
    matchOutputList = get_match_speech_and_card(matches, game_name, True)
    print matchOutputList
    speechOutput += ' '.join([m[0] for m in matchOutputList])
    cardOutput += "\n".join([m[1] for m in matchOutputList])
    speechOutput += "<break strength=\"medium\"/>Would you like to hear more?</speak>"
    return EsportsTicker.tellWithCard(speechOutput, cardTitle, cardOutput, shouldEndSession=False, repromptText="Would you like to hear more matches?")


def get_match_speech_and_card(matches, game_name, with_duration_to_start=False, with_match_results=False):
    matchOutputList = []
    log.info("With Duration {}".format(with_duration_to_start))
    for match in matches:
        matchSpeechOutput = "<p>"
        matchCardOutput = ''
        start_time = dateutil.parser.parse(match.start)
        current_time = datetime.datetime.now(pytz.utc)
        diff = relativedelta(start_time, current_time)
        log.info("START TIME-------------- {}".format(start_time))
        log.info("DURATION {}d{}h{}m".format(diff.days, diff.hours, diff.minutes))
        if not game_name:
            matchCardOutput += "{} - ".format(match._raw_data['tournament']['game']['title'])
            matchSpeechOutput += "{} <break strength=\"medium\"/>".format(match._raw_data['tournament']['game']['long_title'])
        if match.matchups and len(match.matchups) > 0:
            for matchups in match.matchups:
                matchSpeechOutput += get_competitors_speech_string(match, with_match_results)
                matchCardOutput += get_competitors_card_string(match, with_match_results)
        else:
            matchSpeechOutput += ' unknown matchup '
            matchCardOutput += ' Unknown Matchup '
        matchSpeechOutput += "<break strength=\"medium\"/> in {}".format(match._raw_data['tournament']['title'])

        if with_duration_to_start:
            start_time = dateutil.parser.parse(match.start)
            current_time = datetime.datetime.now(pytz.utc)
            diff = relativedelta(start_time, current_time)
            matchSpeechOutput += "<break strength=\"weak\"/> in "
            matchCardOutput += " - "
            add_and = False
            num_entries = 0
            if(diff.days):
                log.debug("Adding days {}".format(diff.days))
                day_or_days = 'day'
                if(diff.days > 1):
                    day_or_days = 'days'
                matchSpeechOutput += "{} {} ".format(diff.days, day_or_days)
                matchCardOutput += "{}d".format(diff.days)
                add_and = True
                num_entries += 1
            if(diff.hours):
                log.debug("Adding hours {}".format(diff.hours))
                hour_or_hours = 'hour'
                if add_and:
                    matchSpeechOutput += "and "
                if(diff.hours > 1):
                    hour_or_hours = 'hours'
                matchSpeechOutput += "{} {} ".format(diff.hours, hour_or_hours)
                matchCardOutput += "{}h".format(diff.hours)
                add_and = True
                num_entries += 1
            if(diff.minutes):
                log.debug("Inside of minutesi {}".format(num_entries))
                if num_entries < 2:
                    if add_and:
                        matchSpeechOutput += "and "
                    minute_or_minutes = 'minute'
                    if(diff.minutes > 1):
                        minute_or_minutes = 'minutes'
                    matchSpeechOutput += "{} {} ".format(diff.minutes, minute_or_minutes)
                    matchCardOutput += "{}m".format(diff.minutes)
        matchSpeechOutput += '</p>'
        matchOutputPair = [matchSpeechOutput, matchCardOutput]
        matchOutputList.append(matchOutputPair)
    return matchOutputList


def lambda_handler(event, context):
    print "lambda handler event: {}".format(event)
    eticker = EsportsTickerAlexaSkill(event=event)
    return eticker.process_event()
